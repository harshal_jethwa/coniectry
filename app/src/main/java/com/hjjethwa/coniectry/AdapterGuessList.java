package com.hjjethwa.coniectry;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.hjjethwa.coniectry.Data.Keys;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by hjjethwa on 9/17/14.
 */
public class AdapterGuessList extends BaseAdapter {

    private static final int PRELOAD_POSITION_3 = 3;
    private static final int PRELOAD_POSITION_4 = 4;
    private static final int PRELOAD_POSITION_5 = 5;

    private final Context mContext;
    private List<String> mData;
    private final int mWidth;
    private final int mHeight;
    private ConcurrentHashMap<Integer, Bitmap> mBitmapCache;

    public AdapterGuessList(Context context, List<String> data) {
        super();
        mContext = context;
        mData = data;
        mBitmapCache = new ConcurrentHashMap<Integer, Bitmap>();

        final WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        mWidth = size.x;
        mHeight = size.y;
    }

    public void addData(List<String> data) {
        for (int i = 0; i < data.size(); i++) {
            mData.add(data.get(i));
        }
        VolleyApplication.setCurrentGuessList(mData);
    }

    public void clearData() {
        mData = new ArrayList<String>();
        mBitmapCache.clear();
    }

    public ConcurrentHashMap<Integer, Bitmap> getmBitmapCache() {
        return mBitmapCache;
    }

    public List<String> getData() {
        return mData;
    }

    public void setBitmapCache(ConcurrentHashMap<Integer, Bitmap> mBitmapCache) {
        this.mBitmapCache = mBitmapCache;
    }

    public void setData(List<String> mData) {
        this.mData = mData;
    }

    private static class ViewHolder {
        ImageView imageView;
        RelativeLayout relativeLayoutSpinner;
        int position;
    }

    @Override
    public int getCount() {
        if (mData != null) {
            return mData.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if (i <= getCount()){
            return mData.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            final LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.guess_list_row, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.row_guess_list_image);
            holder.relativeLayoutSpinner = (RelativeLayout) convertView.findViewById(R.id.row_guess_list_relative_layout_progress);
            final LayoutParams layoutParamsImageView = holder.imageView.getLayoutParams();
            final LayoutParams layoutParamsRelativeLayoutSpinner = holder.relativeLayoutSpinner.getLayoutParams();

            //Setting width and height so that the spinner takes equal space as the image.
            if (mWidth < mHeight) {
                layoutParamsImageView.height = mWidth;
                layoutParamsImageView.width = mWidth;
                layoutParamsRelativeLayoutSpinner.height = mWidth;
                layoutParamsRelativeLayoutSpinner.width = mWidth;
            } else {
                layoutParamsImageView.height = mHeight;
                layoutParamsImageView.width = mHeight;
                layoutParamsRelativeLayoutSpinner.height = mHeight;
                layoutParamsRelativeLayoutSpinner.width = mHeight;
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final String imageUrl = (String) getItem(position);
        holder.position = position;
        if (mBitmapCache.containsKey(position)) {
            holder.imageView.setImageBitmap(mBitmapCache.get(position));
            holder.imageView.setVisibility(View.VISIBLE);
            holder.relativeLayoutSpinner.setVisibility(View.INVISIBLE);
        } else {
            holder.imageView.setImageBitmap(null);
            holder.imageView.setVisibility(View.INVISIBLE);
            holder.relativeLayoutSpinner.setVisibility(View.VISIBLE);
            new GetBitmapTask(holder, position).execute(imageUrl);

            // This check so that we dont try to preload
            // after the last picture.
            if (position + PRELOAD_POSITION_5 < getCount()) {
                preload(position);
            }
        }
//        TextView test = (TextView) convertView.findViewById(R.id.testViewTest);
//        test.setText(imageUrl);
//        holder.imageView.setImageBitmap(null);
        return convertView;

    }

    private void preload(final int position) {
        new PreLoadTask(position + PRELOAD_POSITION_3).execute((String) getItem(position + PRELOAD_POSITION_3));
        new PreLoadTask(position + PRELOAD_POSITION_4).execute((String) getItem(position + PRELOAD_POSITION_4));
        new PreLoadTask(position + PRELOAD_POSITION_5).execute((String) getItem(position + PRELOAD_POSITION_5));
    }

    private void loadImage(String url, final int position) {
        ImageRequest request = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                if (response != null) {
                    mBitmapCache.putIfAbsent(position, response);
                }
            }
        }, 0, 0, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        VolleyApplication.getInstance().getRequestQueue().add(request);
    }

    private class PreLoadTask extends AsyncTask<String, Void, Void> {

        private int position;

        public PreLoadTask(int pos) {
            position = pos;
        }

        @Override
        protected Void doInBackground(String... strings) {
            loadImage(strings[0], position);
            return null;
        }
    }

    private class GetBitmapTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ViewHolder> holderTaskReference;
        private final int postition;

        public GetBitmapTask(ViewHolder holder, int pos) {
            holderTaskReference = new WeakReference<ViewHolder>(holder);
            postition = pos;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {

            final AtomicReference<Bitmap> bitmap = new AtomicReference<Bitmap>();
            final CountDownLatch countDownLatch = new CountDownLatch(1);

            ImageRequest imageRequest = new ImageRequest(strings[0], new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {
                    bitmap.set(response);
                    countDownLatch.countDown();
                }
            }, 0, 0, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    countDownLatch.countDown();
                }
            });
            VolleyApplication.getInstance().getRequestQueue().add(imageRequest);
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return bitmap.get();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            final ViewHolder holder = holderTaskReference.get();
            if (holder != null && bitmap != null && holder.position == postition) {
                mBitmapCache.putIfAbsent(holder.position, bitmap);
                holder.imageView.setImageBitmap(bitmap);
                holder.relativeLayoutSpinner.setVisibility(View.INVISIBLE);
                holder.imageView.setVisibility(View.VISIBLE);
            }
        }
    }
}
