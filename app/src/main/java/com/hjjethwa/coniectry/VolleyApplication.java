package com.hjjethwa.coniectry;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by hjjethwa on 9/14/14.
 */
public class VolleyApplication extends Application {

    private static VolleyApplication mVolleyInstance;

    private RequestQueue mRequestQueue;

    private static List<String> mCurrentGuessList;

    private static ConcurrentHashMap<Integer, Bitmap> mCurrentGuessListCache;

    @Override
    public void onCreate() {
        super.onCreate();
        mRequestQueue = Volley.newRequestQueue(this);
        mVolleyInstance = this;
    }

    public synchronized static VolleyApplication getInstance() {
        return mVolleyInstance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public void stopRequestQueue() {
        mRequestQueue.stop();
    }

    public void startRequestQueue() {
        mRequestQueue.start();
    }

    public static void setCurrentGuessList(List<String> currentGuessList) {
        mCurrentGuessList = currentGuessList;
    }

    public static void setCurrentGuessListCache(ConcurrentHashMap<Integer, Bitmap> currentGuessListCache) {
        mCurrentGuessListCache = currentGuessListCache;
    }

    public static ConcurrentHashMap<Integer, Bitmap> getCurrentGuessListCache() {
        return mCurrentGuessListCache;
    }

    public static List<String> getCurrentGuessList() {
        return mCurrentGuessList;
    }
}
