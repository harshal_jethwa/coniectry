package com.hjjethwa.coniectry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.hjjethwa.coniectry.Data.ChoiceList;
import com.hjjethwa.coniectry.Data.ChoiceList.Choice;
import com.hjjethwa.coniectry.Data.Keys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by hjjethwa on 9/14/14.
 */
public class ActivityGuessList extends Activity {

    private static final String SAVED_RADIO_CHOICE = "saved_radio_choice";
    private static final String SAVED_BUTTON_A_CHOICE = "saved_button_a_choice";
    private static final String SAVED_BUTTON_B_CHOICE = "saved_button_b_choice";
    private static final String SAVED_BUTTON_C_CHOICE = "saved_button_c_choice";
    private static final String SAVED_NEXT_URL = "saved_next_url";
    private static final String SAVED_CURRENT_COUNTRY = "saved_current_country";
    private static final String SAVED_LAST_TOTAL_COUNT = "saved_last_total_count";
    private static final String SAVED_CURRENT_LIST_SCROLL_POSITION = "saved_current_list_scroll_position";
    private static final String SAVED_CURRENT_LIST_SCROLL_FROM_TOP = "saved_current_list_from_top";
    private static final String SAVED_CURRENT_CHOICE = "saved_current_choice";

    private static final String INTENT_EXTRA_CHOICE = "intent_extra_choice";

    private static final int CHOICE_A_INDEX = 0;
    private static final int CHOICE_B_INDEX = 1;
    private static final int CHOICE_C_INDEX = 2;
    private static final int TOTAL_CHOICES = 3;

    private ListView mListView;
    private RadioGroup mRadioGroupChoices;
    private RadioButton mButtonChoiceA;
    private RadioButton mButtonChoiceB;
    private RadioButton mButtonChoiceC;
    private String mNextUrl;
    private String mCurrentCountry;
    private Choice mCurrentChoice;

    private AdapterGuessList mAdapter;

    //This count is for scroll listener.
    //Coz the onScroll is called multiple times
    //we want to add images only once.
    private int mLastTotalCount;

    public static Intent getIntentForSelectedChoiceList (Context context, Choice choice) {
        final Intent intent = new Intent();
        intent.setClass(context, ActivityGuessList.class);
        intent.putExtra(INTENT_EXTRA_CHOICE, choice);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guess_list_activity);
        mListView = (ListView) findViewById(R.id.activity_guess_list_listview);
        mListView.setOnScrollListener(mScrollListener);
        mRadioGroupChoices = (RadioGroup) findViewById(R.id.activity_guess_list_choices_group);
        mButtonChoiceA = (RadioButton) findViewById(R.id.activity_guess_list_choice_button_a);
        mButtonChoiceB = (RadioButton) findViewById(R.id.activity_guess_list_choice_button_b);
        mButtonChoiceC = (RadioButton) findViewById(R.id.activity_guess_list_choice_button_c);
        Button buttonNext = (Button) findViewById(R.id.activity_guess_list_next_button);
        Button buttonSelect = (Button) findViewById(R.id.activity_guess_list_select_button);
        buttonNext.setOnClickListener(mButtonNextOnClickListener);
        buttonSelect.setOnClickListener(mButtonSelectOnClickListener);
        mCurrentChoice = (Choice) getIntent().getSerializableExtra(INTENT_EXTRA_CHOICE);
        mAdapter = new AdapterGuessList(this, new ArrayList<String>());
        mListView.setAdapter(mAdapter);
        if (savedInstanceState != null) {
            mRadioGroupChoices.check(savedInstanceState.getInt(SAVED_RADIO_CHOICE));
            mButtonChoiceA.setText(savedInstanceState.getString(SAVED_BUTTON_A_CHOICE));
            mButtonChoiceB.setText(savedInstanceState.getString(SAVED_BUTTON_B_CHOICE));
            mButtonChoiceC.setText(savedInstanceState.getString(SAVED_BUTTON_C_CHOICE));
            mNextUrl = savedInstanceState.getString(SAVED_NEXT_URL);
            mCurrentCountry = savedInstanceState.getString(SAVED_CURRENT_COUNTRY);
            mLastTotalCount = savedInstanceState.getInt(SAVED_LAST_TOTAL_COUNT);
            mCurrentChoice = (Choice) savedInstanceState.getSerializable(SAVED_CURRENT_CHOICE);
            mAdapter.setData(VolleyApplication.getCurrentGuessList());
            mAdapter.setBitmapCache(VolleyApplication.getCurrentGuessListCache());
            mAdapter.notifyDataSetChanged();
            int listFirstVisiblePosition = savedInstanceState.getInt(SAVED_CURRENT_LIST_SCROLL_POSITION);
            int listFromTop = savedInstanceState.getInt(SAVED_CURRENT_LIST_SCROLL_FROM_TOP);
            mListView.setSelectionFromTop(listFirstVisiblePosition, listFromTop);
        } else {
            refreshViewWithNewChoices();
        }
    }

    private void refreshViewWithNewChoices() {
        mRadioGroupChoices.clearCheck();
        mLastTotalCount = 0;
        final List<String> randomChoices = getAllChoices();
        mButtonChoiceA.setText(randomChoices.get(CHOICE_A_INDEX));
        mButtonChoiceB.setText(randomChoices.get(CHOICE_B_INDEX));
        mButtonChoiceC.setText(randomChoices.get(CHOICE_C_INDEX));
        mCurrentCountry = randomChoices.get(new Random().nextInt(TOTAL_CHOICES));
        mNextUrl = String.format(Keys.INSTAGRAM_SEARCH_BY_TAG, mCurrentCountry.replaceAll("\\s", "").toLowerCase());
        mListView.setSelection(0);
        mAdapter.clearData();
        mAdapter.notifyDataSetInvalidated();
        new GetImagesTask().execute(mNextUrl);
    }

    private List<String> getAllChoices() {
        final List<String> allChoicesList = mCurrentChoice.getChoiceList();
        final int totalChoicesListSize = allChoicesList.size();
        ArrayList<Integer> numbersRandomList = new ArrayList<Integer>(totalChoicesListSize);
        for (int i = 0; i < totalChoicesListSize; i++) {
            numbersRandomList.add(i);
        }
        Collections.shuffle(numbersRandomList);
        final List<String> randomChoices = new ArrayList<String>(3);
        randomChoices.add(CHOICE_A_INDEX, allChoicesList.get(numbersRandomList.get(CHOICE_A_INDEX)));
        randomChoices.add(CHOICE_B_INDEX, allChoicesList.get(numbersRandomList.get(CHOICE_B_INDEX)));
        randomChoices.add(CHOICE_C_INDEX, allChoicesList.get(numbersRandomList.get(CHOICE_C_INDEX)));
        return randomChoices;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_RADIO_CHOICE, mRadioGroupChoices.getCheckedRadioButtonId());
        outState.putString(SAVED_BUTTON_A_CHOICE, mButtonChoiceA.getText().toString());
        outState.putString(SAVED_BUTTON_B_CHOICE, mButtonChoiceB.getText().toString());
        outState.putString(SAVED_BUTTON_C_CHOICE, mButtonChoiceC.getText().toString());
        outState.putString(SAVED_NEXT_URL, mNextUrl);
        outState.putString(SAVED_CURRENT_COUNTRY, mCurrentCountry);
        outState.putInt(SAVED_LAST_TOTAL_COUNT, mLastTotalCount);
        outState.putSerializable(SAVED_CURRENT_CHOICE, mCurrentChoice);
        outState.putInt(SAVED_CURRENT_LIST_SCROLL_POSITION, mListView.getFirstVisiblePosition());
        View lv = mListView.getChildAt(0);
        outState.putInt(SAVED_CURRENT_LIST_SCROLL_FROM_TOP, lv == null ? 0 : lv.getTop());
        VolleyApplication.setCurrentGuessList(mAdapter.getData());
        VolleyApplication.setCurrentGuessListCache(mAdapter.getmBitmapCache());
    }

    private class GetImagesTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... strings) {
            final AtomicReference<JSONObject> jsonObject = new AtomicReference<JSONObject>();
            final CountDownLatch countDownLatch = new CountDownLatch(1);

            JsonObjectRequest request = new JsonObjectRequest(strings[0], null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    jsonObject.set(response);
                    countDownLatch.countDown();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    countDownLatch.countDown();
                }
            });
            VolleyApplication.getInstance().getRequestQueue().add(request);
            try {
                countDownLatch.await();
            } catch (Exception error) {
                error.printStackTrace();
            }
            return jsonObject.get();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                mNextUrl = jsonObject.getJSONObject(Keys.PAGINATION).getString(Keys.NEXT_URL);
                final JSONArray data = jsonObject.getJSONArray(Keys.DATA);
                final int dataLength = data.length();
                final List<String> imageUrls = new ArrayList<String>(dataLength);
                for (int i = 0; i < dataLength; i++) {
                    imageUrls.add(data.getJSONObject(i).getJSONObject(Keys.IMAGES).getJSONObject(Keys.STANDARD_RESOLUTION).getString(Keys.URL));
                }
                mAdapter.addData(imageUrls);
                mAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private final OnClickListener mButtonSelectOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final int radioButtonSelectedID = mRadioGroupChoices.getCheckedRadioButtonId();
            final RadioButton radioButtonSelectedRadioButton = (RadioButton) mRadioGroupChoices.findViewById(radioButtonSelectedID);
            if (radioButtonSelectedRadioButton != null) {
                if (radioButtonSelectedRadioButton.getText().equals(mCurrentCountry)) {
                    refreshViewWithNewChoices();
                }
            }
        }
    };

    private final OnClickListener mButtonNextOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            refreshViewWithNewChoices();
        }
    };

    private final OnScrollListener mScrollListener = new OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView absListView, int scrollState) {
            // Doing this so that the list view scrolls smoothly.
            if (scrollState == OnScrollListener.SCROLL_STATE_FLING) {
                VolleyApplication.getInstance().stopRequestQueue();
            } else {
                VolleyApplication.getInstance().startRequestQueue();
            }
        }

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0 && totalItemCount != mLastTotalCount) {
                new GetImagesTask().execute(mNextUrl);
                mLastTotalCount = totalItemCount;
            }
        }
    };
}
