package com.hjjethwa.coniectry.Data;

/**
 * Created by hjjethwa on 9/14/14.
 */
public class Keys {

    public static final String INSTAGRAM_CLIENT_ID = "abc1ebd47a594c399e460d27cd6eff4c";
    public static final String INSTAGRAM_CLIENT_SECRET = "5ade62998650481db96bb9c2431da96c";
    public static final String INSTAGRAM_URL_API = "https://api.instagram.com/v1";
    public static String INSTAGRAM_URL_CALLBACK = "http://www.hjjethwa.com";

    public static final String INSTAGRAM_URL_MEDIA_SEARCH = "https://api.instagram.com/v1/media/search?";
    public static final String INSTAGRAM_SEARCH_BY_TAG = INSTAGRAM_URL_API + "/tags/%s/media/recent?client_id=" + INSTAGRAM_CLIENT_ID;

    public static final String DATA = "data";
    public static final String IMAGES = "images";
    public static final String STANDARD_RESOLUTION = "standard_resolution";
    public static final String URL = "url";
    public static final String PAGINATION = "pagination";
    public static final String NEXT_URL = "next_url";
}
