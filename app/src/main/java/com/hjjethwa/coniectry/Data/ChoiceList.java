package com.hjjethwa.coniectry.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hjjethwa on 9/27/14.
 */

public class ChoiceList {

    public enum Choice {
        US_STATE(mUSStatesList),
        ONTARIO_CITIES(mOntarioCitiesList);

        private List<String> mChoiceList;

        Choice(List<String> choiceList) {
            mChoiceList = choiceList;
        }

        public List<String> getChoiceList() {
            return mChoiceList;
        }
    }

    private static List<String> mUSStatesList = new ArrayList<String>(50) {
        {
            add("Alabama");
            add("Alaska");
            add("Arizona");
            add("Arkansas");
            add("California");
            add("Colorado");
            add("Connecticut");
            add("Delaware");
            add("Florida");
            add("Georgia");
            add("Hawaii");
            add("Idaho");
            add("Illinois");
            add("Indiana");
            add("Iowa");
            add("Kansas");
            add("Kentucky");
            add("Louisiana");
            add("Maine");
            add("Maryland");
            add("Massachusetts");
            add("Michigan");
            add("Minnesota");
            add("Mississippi");
            add("Missouri");
            add("Montana");
            add("Nebraska");
            add("Nevada");
            add("New Hampshire");
            add("New Jersey");
            add("New Mexico");
            add("New York");
            add("North Carolina");
            add("North Dakota");
            add("Ohio");
            add("Oklahoma");
            add("Oregon");
            add("Pennsylvania");
            add("Rhode Island");
            add("South Carolina");
            add("South Dakota");
            add("Tennessee");
            add("Texas");
            add("Utah");
            add("Vermont");
            add("Virginia");
            add("Washington");
            add("West Virginia");
            add("Wisconsin");
            add("Wyoming");
        }
    };

    private static List<String> mOntarioCitiesList = new ArrayList<String>(35) {
        {
            add("Barrie");
            add("Belleville");
            add("Brampton");
            add("Brant");
            add("Brantford");
            add("Brockville");
            add("Burlington");
            add("Cambridge");
            add("Cornwall");
            add("Dryden");
            add("Guelph");
            add("Hamilton");
            add("Kenora");
            add("Kingston");
            add("Kitchener");
            add("Markham");
            add("Mississauga");
            add("Niagara Falls");
            add("Norfolk County");
            add("North Bay");
            add("Orillia");
            add("Oshawa");
            add("Ottawa");
            add("Owen Sound");
            add("Pembroke");
            add("Peterborough");
            add("Pickering");
            add("Port Colborne");
            add("Prince Edward County");
            add("Sarnia");
            add("Stratford");
            add("Toronto");
            add("Vaughan");
            add("Waterloo");
            add("Windsor");
            add("WoodStock");
        }
    };
}